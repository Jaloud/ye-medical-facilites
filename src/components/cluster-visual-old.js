
import React, { useRef, useEffect } from "react";
// import incidentData from "../data/Medical_facility 2019 Yemen - Incident Master";

import * as d3 from "d3";
const jsonData = require ('../data/Medical_facility 2019 Yemen - Incident Master.json');
const ClusterVisual = ({ collectionName, lang = "en"}) => {
  const svgEl = useRef(null); //set reference
  const svgElLegend = useRef(null)

let rawData = [];
const width = 1260; //move to props
const height = 800; //move to props
const margin = { top: 5, right: 20, bottom: 30, left: 20 };
const svgWidth = width - margin.right - margin.left;
const svgHeight = height - margin.top - margin.bottom;
let color = d3.scaleOrdinal(d3.schemeSet3);
  useEffect(() => {
      rawData = null;
      //Get unique set of perpetrators
      const perpetrators = jsonData.map(dataObject => {
        return dataObject["alleged perpetrator (general0.0)"]
      }).filter(perpetrator => {
        return perpetrator !== "UNKNOWN" && perpetrator
      })
      console.log(perpetrators)
      const uniquePerps = perpetrators.reduce((unique, item) => unique.includes(item) ? unique : [...unique, item], [])
      console.log(uniquePerps)
      const children = [];
      uniquePerps.forEach(perpetrator => {
        console.log("there should be several", perpetrator)
        const perpsFormatted =  jsonData.map(dataPoint => {
          // console.log(dataPoint)
          if (dataPoint["alleged perpetrator (general0.0)"] === perpetrator) {
            console.log("getting here!!!")
            const formattedData =  {
              code: dataPoint["Code "],
              hospital: dataPoint["name of the hospital_en"],
              location: dataPoint["location . Analysis"],
              perpetrator: dataPoint["alleged perpetrator (general0.0)"]
            }
            // console.log(formattedData)
            return formattedData
          }
          
        }).filter(dataPoint => {
          return dataPoint !== undefined
        })
        
        children.push({
          name: perpetrator,
          children: perpsFormatted
        })
      })
      console.log("is this formatted", children)
      const finalData = {
        children: children
      }
      console.log("the final data", finalData)

      // {
      //   children: [
      //     {
      //       name: "Pro- Coalition forces",
      //       children: [
      //         {
      //           "code": "Code",
      //           "hospital": "name of the hospital_en",
      //           "location": "location . Analysis"
      //         }
      //       ]
      //     }
      //   ]
      // }

    //   console.log(Array.isArray(jsonData), "is this coming through")
    //   const perpetratorData = jsonData.filter(dataObject => {
    //     return dataObject["alleged perpetrator (general0.0)"] && dataObject["alleged perpetrator (general0.0)"] !== "UNKNOWN"
    //   }).map(dataObject => {
    //     return {
    //       code: dataObject["Code "],
    //       allegedPerpetrator: dataObject["alleged perpetrator (general0.0)"]
    //     }
    //   })
    //   console.log(perpetratorData)


    d3.json('https://cdn.rawgit.com/freeCodeCamp/testable-projects-fcc/a80ce8f9/src/data/tree_map/movie-data.json').then((data) => {
        rawData = data;
        drawTreemap(rawData);
    })
  }, []);

  // function addLabel(d,i){
  //   //let color = d3.scaleOrdinal(d3.schemeSet3);
  //   const l = d3.select(svgElLegend);
  //   l.append('rect').style('fill', d => color(d))
  //     .attr('class', 'legend')
  //     .attr('x', 20)
  //     .attr('y', i * 10 + 30)
  //     .attr('width', 36)
  //     .attr('height', 15)
  //     .attr('transform', `translate(0, ${i * 10 + 10})`);
  //   l.append('text')
  //     .attr('transform', `translate(0, ${i * 10 + 20})`)
  //     .attr('font-size', '.9rem').append('tspan')
  //     .attr('x', 65).attr('y', i * 10 + 33)
  //     .html(d);
  // }

  function name(d) {
    const p = d3.select(this);
    const a = d.data.name.trim();
    const b = a.split(' ');
    let e = 0;
    for (let c = 0; c < b.length; c++) {
      e += 10;
      p.append('tspan')
        .attr('x', 4)
        .attr('y', e)
        .html(b[c]);
    }
  }

  function drawTreemap(d) {
    console.log("the data here at treemap:", d)
    const uniqueElements = arr => [...new Set(arr)];
      //create treepmap:
      const treemap = d3.treemap().size([width, height]);
      const diagram = d3.select(svgEl.current)
      .attr("width", svgWidth + margin.left + margin.right)
      .attr("height", svgHeight + margin.top + margin.bottom);

    let catg = [];
    const div = d3.select("body").append("div")
    .style("opacity", 0);
    const root = d3.hierarchy(d);
    console.log("THE ROOT:", root)
    root.sum((d) => d.value);
    treemap(root);

    const nodes = diagram.selectAll('rect')
    .data(root.descendants())
    .enter().append('g').attr("transform", d => {
        const x = d.x0;
        const y = d.y0;
        return `translate(${x}, ${y})`;
      }).attr('class', 'group');
      nodes.append('rect').style('fill', (d) =>  color((d.children ? d : d.parent).data.name))
    .attr('ui', d => d.data.name.trim())
    .attr("stroke", "#404040")
    .attr("stroke-width", .3)
    .attr('class', 'cell')
    .attr('width', (d) => d.x1 - d.x0)
    .attr('height', (d) => d.y1 - d.y0)
    .each(d => {
      catg.push(d.data.category ? d.data.category : "Comedy");
    })
    // .on("mouseover", mouseover)
    // .on("mouseout", mouseout).each(d => {   
    //   catg.push(d.data.category ? d.data.category : "Comedy");
    // });
    /*
    Commentings out mouseover and mouseout for now. 
    */
  
  const cat = uniqueElements(catg);
  console.log(cat, "the cat")

 nodes.append('text')
    .attr('font-size', '.6rem')
    .attr('class', 'text')
    .each(name);

    const legend = d3.select(svgElLegend.current)
        .attr('width', 200)
        .attr('height', 175)
        .attr("id","legend");

        legend.append('text').html('Legend').attr("transform","translate(50,20)");

  legend.selectAll('rect')
        .data(cat).enter().append('g').attr("transform","translate(10,0)").each((d, i) => {
          let color = d3.scaleOrdinal(d3.schemeSet3);
          const l = d3.select(svgElLegend.current);
          l.append('rect').style('fill', d => color(d))
            .attr('class', 'legend')
            .attr('x', 20)
            .attr('y', i * 10 + 30)
            .attr('width', 36)
            .attr('height', 15)
            .attr('transform', `translate(0, ${i * 10 + 10})`);
          l.append('text')
            .attr('transform', `translate(0, ${i * 10 + 20})`)
            .attr('font-size', '.9rem').append('tspan')
            .attr('x', 65).attr('y', i * 10 + 33)
            .html(d);

        });


  }

  return <div>
      <svg ref={svgEl} width={300} height={300}></svg>
      <div>
      <svg ref={svgElLegend} width={100} height={500}></svg>
      </div>
      
  </div>
  
};

export default ClusterVisual;
