import React, { useState, useContext, useEffect, Fragment } from "react";
import { useStaticQuery, graphql, Link } from "gatsby";
import { LocaleContext } from "../context/locale-context";
import LocalizedLink from "../components/localizedLink";
import useTranslations from "../components/hooks/useTranslations";
import { jsx } from "@emotion/core";
import styled from "@emotion/styled";
import { rhythm, scale } from "../utils/typography";
import useWindowSize from "../components/hooks/useWindowSize";

const Item = styled.li({
  margin: 0,
  paddingLeft: rhythm(2),
  "& a": {
    color: "white",
    fontWeight: 500
  }
});
let image;

const assignActiveStyles = ({ isPartiallyCurrent }) =>
  isPartiallyCurrent ? { style: { color: "#FF5400" } } : {};

function Navigation() {
  const locale = useContext(LocaleContext);
  const [windowWidthSize, windowHeightSize] = useWindowSize();
  const [isMobile, setIsMobile] = useState(false);
  const tr = useTranslations();
  const isRTL = locale === "ar";
  useEffect(() => {
    windowWidthSize < 992 ? setIsMobile(true) : setIsMobile(false);
  }, [windowWidthSize]);
  return (
    <nav
      css={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        margin: `0.7rem 4rem`,
        textTransform: "uppercase"
      }}
    >
      <a href="https://yemeniarchive.org/en">
        <img
          src="/assets/logos/logo.svg"
          height="100%"
          css={{ marginBottom: 0 }}
        />
      </a>

      <ul
        css={{
          display: "flex",
          alignItems: "center",
          listStyleType: "none",
          margin: 0
        }}
      >
        <Item>
          <a href="https://yemeniarchive.org/en/investigations">
            {tr("Investigations")}
          </a>
        </Item>
        <Item>
          <a href="https://yemeniarchive.org/en/database">
            {tr("Data Archive")}
          </a>
        </Item>
        <Item>
          <a href="https://yemeniarchive.org/en/tools_methods">
            {tr("Tools and Methods")}
          </a>
        </Item>
        <Item>
          <a href="https://yemeniarchive.org/en/tech-advocacy">
            {tr("Tech Advocacy")}
          </a>
        </Item>
        <Item>
          <a href="https://yemeniarchive.org/en/about">{tr("About")}</a>
        </Item>
      </ul>
    </nav>
  );
}

function Header() {
  const data = useStaticQuery(graphql`
    query HeaderQuery {
      file(name: { eq: "headerImage" }) {
        id
        childImageSharp {
          fluid(quality: 75, maxWidth: 1200) {
            src
            ...GatsbyImageSharpFluid
            originalName
            originalImg
          }
        }
      }
    }
  `);
  image = data.file.childImageSharp.fluid;
  const [isScrolled, setIsScrolled] = useState(false);
  const [isMobile, setIsMobile] = useState(false);
  const [windowWidthSize, windowHeightSize] = useWindowSize();
  const locale = useContext(LocaleContext);
  const isRTL = locale === "ar";
  const tr = useTranslations();
  const checkNav = () => {
    if (window.pageYOffset > 10) {
      setIsScrolled(true);
    }
  };

  if (typeof window !== `undefined`) {
    useEffect(() => {
      windowWidthSize < 992 ? setIsMobile(true) : setIsMobile(false);
      window.addEventListener("scroll", checkNav);
    }, [window.pageYOffset, windowWidthSize]);
  }

  return (
    <Fragment>
      {isMobile && (
        <div css={{ backgroundColor: "black" }}>
          <a href="https://yemeniarchive.org/en">
            <img
              src="/assets/logos/logo.svg"
              height="100%"
              css={{ marginBottom: 0 }}
            />
          </a>
        </div>
      )}
      <div
        css={{
          position: "sticky",
          top: 0
        }}
      >
        <div
          css={{
            height:
              isMobile && !isScrolled
                ? "30vh"
                : isMobile && isScrolled
                ? "20vh"
                : isScrolled
                ? "10vh"
                : "50vh",
            backgroundImage: `url(${image.src})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center center",
            backgroundAttachment: "fixed",
            position: "relative",
            borderBottom: "3px #b32c50 solid",
            direction: isRTL ? "rtl" : "ltr"
          }}
        >
          <div
            css={{
              position: "absolute",
              top: "0",
              left: "0",
              width: "100%",
              height: "100%",
              backgroundColor: "rgba(10, 10, 10, 0.3)",
              display: isMobile ? "flex" : isScrolled && "flex",
              justifyContent: "space-around"
            }}
          >
            <div>
              {isMobile || isScrolled || <Navigation />}
              <div
                css={{
                  maxWidth: isScrolled ? "100%" : "90%",
                  margin: "0 auto",
                  textAlign: "center",
                  height: isScrolled ? "100%" : "auto",
                  display: isMobile ? "flex" : isScrolled ? "flex" : "block",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <h1
                  css={{
                    color: "white",
                    textShadow: "0 2px 4px rgba(0,0,0,0.5)",
                    margin: isScrolled ? "0" : "auto",
                    marginTop: isScrolled ? "0" : rhythm(2),
                    fontSize: isMobile ? "2rem" : isScrolled ? "2rem" : "4rem",
                    display: isScrolled ? isMobile && "none" : "auto"
                  }}
                >
                  {tr("Medical Facilites Under Fire")}
                </h1>
              </div>
            </div>
            <div
              css={{
                display: isScrolled ? "flex" : "block",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <table
                css={{
                  maxWidth: rhythm(27),
                  margin: "0 auto",
                  marginTop: isMobile ? 0 : isScrolled ? 0 : rhythm(2),
                  direction: isRTL ? "rtl" : "ltr",
                  borderCollapse: "collapse",
                  fontSize: isScrolled ? "1rem" : "80%",
                  textTransform: "uppercase",
                  color: "white",
                  "& th": {
                    padding: `${rhythm(0.5)} ${rhythm(0.7)}`,
                    textAlign: "center",
                    border: isMobile
                      ? "none"
                      : isScrolled
                      ? "none"
                      : "3px solid white"
                  },
                  " & a": {
                    boxSizing: "border-box",
                    color: "white",
                    borderBottom: "3px solid #b32c50"
                  },
                  "& a:hover": {
                    borderBottom: "6px solid #b32c50"
                  }
                }}
              >
                {isMobile && isScrolled ? (
                  <h1
                    css={{
                      color: "white",
                      textShadow: "0 2px 4px rgba(0,0,0,0.5)",
                      fontSize: "1rem",
                      margin: "0.5rem",
                      textAlign: "center"
                    }}
                  >
                    {tr("Medical Facilites Under Fire")}
                  </h1>
                ) : (
                  <div></div>
                )}
                <tbody>
                  <tr
                    css={{
                      display: isMobile ? "flex" : "auto",
                      flexWrap: "wrap"
                    }}
                  >
                    <th
                      css={{
                        flex: "50%"
                      }}
                    >
                      {isRTL && <Link to="/ar">{tr("Key Findings")}</Link>}
                      {isRTL || <Link to="/">{tr("Key Findings")}</Link>}
                    </th>
                    <th
                      css={{
                        flex: "50%"
                      }}
                    >
                      <LocalizedLink to="/investigations">
                        {tr("Investigations")}
                      </LocalizedLink>
                    </th>
                    <th
                      css={{
                        flex: "50%"
                      }}
                    >
                      <LocalizedLink to="/data-set">
                        {tr("Data Set")}
                      </LocalizedLink>
                    </th>
                    <th
                      css={{
                        flex: "50%"
                      }}
                    >
                      <LocalizedLink to="/method">
                        {tr("methodology")}
                      </LocalizedLink>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default Header;
