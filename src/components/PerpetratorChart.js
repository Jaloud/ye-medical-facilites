import React, { useRef, useEffect } from "react";
import d3 from "../utils/d3-importer";
import { groupBy } from "lodash";
import { format, parse } from "date-fns";
import { colorMarker } from "./YemeniMap";

function PerpetratorChart({ data }) {
  const svgEl = useRef(null);
  useEffect(() => {
    const isLoading = data.length > 0;
    isLoading && drawChart();
  }, [data.length]);

  function drawChart() {
    const finalData = [];
    const margin = { top: 5, right: 20, bottom: 30, left: 20 };
    const width = 800;
    const height = 500;
    const svgWidth = width - margin.right - margin.left;
    const svgHeight = height - margin.top - margin.bottom;
    const radius = Math.min(width, height) / 2;
    let units = groupBy(data, value => value.alleged_perpetrator);
    for (const unit in units) {
      finalData.push({
        perpetrator: unit,
        counts: Object.values(units[unit]).length,
        incidents: Object.values(units[unit])
      });
    }
    const chart = d3
      .select(svgEl.current)
      .attr("width", svgWidth + margin.left + margin.right)
      .attr("height", svgHeight + margin.top + margin.bottom)
      .append("g")
      .attr("transfrom", "translate(400,250)");

    const color = d3
      .scaleOrdinal()
      .domain(data)
      .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56"]);

    const pie = d3.pie().value(d => d.counts);
    const chartData = pie(finalData);
    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    chart
      .selectAll("arc")
      .data(chartData)
      .enter()
      .append("path")
      .attr(
        "d",
        d3
          .arc()
          .innerRadius(0)
          .outerRadius(radius)
      )
      .attr("fill", d => colorMarker(d.data.perpetrator))
      .attr("stroke", "black")
      .style("stroke-width", "2px")
      .style("opacity", 0.7);
  }

  return <svg ref={svgEl}></svg>;
}

export default PerpetratorChart;
