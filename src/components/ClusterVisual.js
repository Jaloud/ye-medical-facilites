import React, { useRef, useEffect } from "react";
import colorMapper from "../utils/color-mapper";
import formatJsonData from "../utils/formatter";
import d3 from "../utils/d3-importer";
const jsonData = require("../../data/data.json"); // Whitney: pass the data from index.js page

const ClusterVisual = ({ collectionName, lang = "en" }) => {
  const svgEl = useRef(null); //set reference for treemap
  const svgElLegend = useRef(null); //set reference for legend

  const width = 800; //move to props
  const height = 600; //move to props
  const margin = { top: 5, right: 20, bottom: 30, left: 20 };
  const svgWidth = width - margin.right - margin.left;
  const svgHeight = height - margin.top - margin.bottom;
  let additionalColors = {};

  useEffect(() => {
    const finalData = formatJsonData(jsonData);
    drawTreemap(finalData);
  }, []);

  //FUNCTIONS FOR MOUSEOVER/MOUSEOUT:
  // function addLabel(d,i){
  //   //let color = d3.scaleOrdinal(d3.schemeSet3);
  //   const l = d3.select(svgElLegend);
  //   l.append('rect').style('fill', d => color(d))
  //     .attr('class', 'legend')
  //     .attr('x', 20)
  //     .attr('y', i * 10 + 30)
  //     .attr('width', 36)
  //     .attr('height', 15)
  //     .attr('transform', `translate(0, ${i * 10 + 10})`);
  //   l.append('text')
  //     .attr('transform', `translate(0, ${i * 10 + 20})`)
  //     .attr('font-size', '.9rem').append('tspan')
  //     .attr('x', 65).attr('y', i * 10 + 33)
  //     .html(d);
  // }

  function name(d) {
    const p = d3.select(this);
    const a = d.data.name.trim();
    const b = a.split(" ");
    let e = 0;
    for (let c = 0; c < b.length; c++) {
      e += 10;
      p.append("tspan")
        .attr("x", 4)
        .attr("y", e)
        .html(b[c]);
    }
  }

  function generateColor(data) {
    const color = d3.scaleOrdinal(d3.schemeSet3);
    const colorFill = color(data.name);
    const perpetrator = data.perpetrator;
    additionalColors[perpetrator] = colorFill;
    return colorFill;
  }

  function determineColor(d) {
    return additionalColors[d.data.perpetrator]
      ? additionalColors[d.data.perpetrator]
      : generateColor(d.data);
  }

  function drawTreemap(d) {
    const uniqueElements = arr => [...new Set(arr)];
    //create treepmap:
    const treemap = d3.treemap().size([width, height]);
    const diagram = d3
      .select(svgEl.current)
      .attr("width", svgWidth + margin.left + margin.right)
      .attr("height", svgHeight + margin.top + margin.bottom);

    let catg = [];
    let colors = [];
    const div = d3
      .select("body")
      .append("div")
      .style("opacity", 0);
    const root = d3.hierarchy(d);
    root.sum(d => d.value);
    treemap(root);

    const nodes = diagram
      .selectAll("rect")
      .data(root.descendants())
      .enter()
      .append("g")
      .attr("transform", d => {
        const x = d.x0;
        const y = d.y0;
        return `translate(${x}, ${y})`;
      })
      .attr("class", "group");
    nodes
      .append("rect")
      .style("fill", d => (d.data.color ? d.data.color : determineColor(d)))
      .attr("ui", d => d.data.name.trim())
      .attr("stroke", "#404040")
      .attr("stroke-width", 0.3)
      .attr("class", "cell")
      .attr("width", d => d.x1 - d.x0)
      .attr("height", d => d.y1 - d.y0)
      .each(d => {
        catg.push(d.data.perpetrator);
      });
    // .on("mouseover", mouseover)
    // .on("mouseout", mouseout).each(d => {
    //   catg.push(d.data.category ? d.data.category : "Comedy");
    // });
    /*
    Commentings out mouseover and mouseout for now. 
    */

    const cat = uniqueElements(catg).filter(el => {
      return el !== undefined;
    });

    nodes
      .append("text")
      .attr("font-size", ".6rem")
      .attr("class", "text")
      .each(name);

    const legend = d3
      .select(svgElLegend.current)
      .attr("width", 1000)
      .attr("height", 175)
      .attr("id", "legend");

    legend
      .append("text")
      .html("Legend")
      .attr("transform", "translate(50,20)");

    legend
      .selectAll("rect")
      .data(cat)
      .enter()
      .append("g")
      .attr("transform", "translate(10,0)")
      .each((d, i) => {
        const hexColor = colorMapper[d] ? colorMapper[d] : additionalColors[d];
        const l = d3.select(svgElLegend.current);
        l.append("rect")
          .style("fill", hexColor)
          .attr("class", "legend")
          .attr("x", 20)
          .attr("y", i * 10 + 30)
          .attr("width", 36)
          .attr("height", 15)
          .attr("transform", `translate(0, ${i * 10 + 10})`);
        l.append("text")
          .attr("transform", `translate(0, ${i * 10 + 20})`)
          .attr("font-size", ".9rem")
          .append("tspan")
          .attr("x", 65)
          .attr("y", i * 10 + 33)
          .html(d);
      });
  }

  return (
    <div>
      <svg ref={svgEl} width={300} height={300}></svg>
      <div>
        <svg ref={svgElLegend} width={100} height={500}></svg>
      </div>
    </div>
  );
};

export default ClusterVisual;
