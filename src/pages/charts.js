import React, { useState, useEffect, useContext, Fragment } from "react";
import TimeLine from "../components/TimeLine";
import ClusterVisual from "../components/ClusterVisual";
import yemeniJson from "../../data/data.json";
import PieChart from "react-minimal-pie-chart";
// import PerpetratorChart from "../components/PerpetratorChart";
import DataBlock from "../components/DataBlock";

const data = [
  {
    key: "Facebook Posts",
    value: 596,
    color: "DodgerBlue"
  },
  {
    key: "Youtube Videos",
    value: 159,
    color: "Tomato"
  },
  {
    key: "Tweets",
    value: 526,
    color: "#0067fc"
  },
  {
    key: "Articles",
    value: 474,
    color: "Orange"
  }
];

function Charts() {
  return (
    <Fragment>
      <h1>Data Set Block</h1>
      <DataBlock data={data} />
      <br />
      <h1>Time line of incidents</h1>
      <TimeLine />
      <br />
      <h1>Cluster Visual</h1>
      <ClusterVisual />
      <br />
      <h1>Perpetrator Chart</h1>
      <PieChart
        data={[
          { title: "AQAP", value: 2, color: "#E38627" },
          { title: "UNKNOWN", value: 2, color: "#383a42" },
          { title: "Pro- Coalition forces", value: 2, color: "#6A2135" },
          { title: "Houthis rebel movement", value: 43, color: "#a626a4" },
          { title: "Coalition led by Sudia", value: 72, color: "#C13C37" }
        ]}
        style={{
          height: "500px"
        }}
      />
    </Fragment>
  );
}

export default Charts;
