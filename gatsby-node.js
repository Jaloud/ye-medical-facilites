const path = require(`path`);
const locales = require(`./config/i18n`);
const { createFilePath } = require(`gatsby-source-filesystem`);

const {
  findKey,
  removeTrailingSlash
} = require(`./src/utils/gatsby-node-helpers`);

exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions;
  deletePage(page);

  Object.keys(locales).map(lang => {
    const localizedPath = (function() {
      if (locales[lang].default && page.path === "/") {
        return "/";
      } else {
        return `${locales[lang].path}${page.path}`;
      }
    })();
    return createPage({
      ...page,
      path: removeTrailingSlash(localizedPath),
      context: {
        ...page.context,
        locale: lang,
        dateFormat: locales[lang].dateFormat
      }
    });
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  //  fmImagesToRelative(node); // convert image paths for gatsby images
  if (node.internal.type === "Mdx") {
    const name = path.basename(node.fileAbsolutePath, `.md`);
    const defaultKey = findKey(locales, o => o.default === true);
    const isDefault = !(name.slice(-3) === "_ar");
    const lang = isDefault ? defaultKey : name.split(`_`)[1];
    createNodeField({ node, name: `locale`, value: lang });
    createNodeField({ node, name: `isDefault`, value: isDefault });

    let value = createFilePath({ node, getNode });
    value = value.includes("_") ? `${value.slice(0, -4)}` : value;
    if (value.includes("index") && !value.includes("html")) {
      value = `${value}.html`;
    }
    createNodeField({
      name: `slug`,
      node,
      value
    });
  }
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage, createRedirect } = actions;
  createRedirect({ fromPath: "/en", toPath: "/", isPermanent: true });
  const pageTemplate = path.resolve(`./src/templates/page.js`);
  return graphql(
    `
      {
        allMdx {
          edges {
            node {
              fields {
                slug
                locale
              }
              frontmatter {
                title
                lang
              }
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors;
    }

    const posts = result.data.allMdx.edges;

    posts.forEach(post => {
      const slug = post.node.fields.slug;
      const title = post.node.frontmatter.title;
      const locale = post.node.frontmatter.lang;
      const path = `${locale}${post.node.fields.slug}`;
      createPage({
        path: path,
        component: pageTemplate,
        context: {
          locale,
          title,
          slug: slug
        }
      });
    });
    return null;
  });
};

// Adding this, so Gatsby-mdx could import js components from src
// Issue number #176 on github
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"]
    }
  });
};
