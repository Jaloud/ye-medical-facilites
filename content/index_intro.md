---
page: index
name: intro
---

The Yemeni Archive aims to create counter narratives to the disinformation created by all actors during the conflict in Yemen through supporting all forms of independent media in their efforts to report and document human rights violations by developing open-source tools, replicable methods, and a publicly-available verified database for use in investigative journalism reports, criminal case building, and evidence-based advocacy campaigns
