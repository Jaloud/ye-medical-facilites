---
page: index
name: victims-one
title: Victims
lang: en
---

## Victims

Yemeni Archive has found documentation that 59 incidents resulted in civilian casualties. Of these, we have been able to confirm the number of victims from 35 of the attacks. Non-combatants, including women, children and medical staff, were killed and dozens were injured. We were additionally able to confirm that an additional 24 attacks resulted in civilian casualties, however we were unable to determine the exact number of victims using open-source techniques due to the conflicting information in numbers about the attacks on medical facilities. For example, for several attacks the casualty figures reported a total number of victims of all attacks on one day, including casualties in medical facilities and centers, but also markets and civilian homes. In 74 attacks, no casualties were reported.
