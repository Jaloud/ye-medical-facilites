---
page: index
name: intro-part-two
title: Added Value
lang: en
---
Yemeni Archive has sought to be detailed and transparent in the presentation process, taking into account the need to protect those presenting these relevant documents. Taking these interests seriously, some of the detailed information methodologies that would deem sensitive will not be published.

However, while all efforts have been made to provide a thorough understanding of incidents and attacks, some information has been withheld to protect the security of our sources.  

To provide Yemeni Archive with new evidence or information, flag a mistake or share any questions or concerns, please contact info@yemeniarchive.org

## Acknowledgements

The videos in this database have been collected by many groups and individuals in Yemen. Currently in this database there are 1755 different pieces of data This collection would not be possible without the brave work of those documenting and reporting on war crimes and crimes against humanity. Our thanks goes to each and every one of them.

Our team would like to thank all those who helped in verifying, annotating and analysing this data. In particular, we would like to thank the student groups at Taiz University for their work verifying and annotating open source content and contributing to investigations. In particular we would like to thank Abdullah Al Mamari and Olfat A. We would also like to thank Mansour Omari, Lilas Al Boni, Sawsan Abdul-Jalil ,Obieda Falhha, Abeer Mohsen.

This project was made possible by International Media Support (IMS), Check Global - MENA IF  through their grant to support students  trained by Yemeni Archive.
