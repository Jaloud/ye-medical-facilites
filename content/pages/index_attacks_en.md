---
page: index
name: attacks
title: Attacks on medical facilities in Yemen
lang: en
---
import TimeLine from '../../src/components/TimeLine.js';

## Attacks on medical facilities in Yemen

Since 2015, Yemen has witnessed a civil war in which regional and global powers have intervened to prop up various local parties to the conflict. The war has resulted in thousands of civilian deaths and injuries and the displacement of hundreds of thousands of civilians. Throughout the war, many civilian objects, including homes, schools, hospitals and vital infrastructure, were targeted by all parties of the conflict.

Attacks on medical facilities, medical and health workers and patients are prohibited by international humanitarian law and constitute war crimes. The Geneva Conventions of 1949 and the Additional Protocols of 1977 mandate the protection of health care facilities and health care workers. Medical facilities are protected by virtue of their function but also because they are run by civilians and civilians are likely to be present in large numbers, especially during times of conflict.

The only exception to this rule is when these medical facilities are used for military purposes, for example as weapons storage facilities or military bases from which missiles are launched. Even then, Article 19 of the First Geneva Conventions stipulates that such protection may “cease only after due warning has been given, naming in all appropriate cases a reasonable time limit and after such warning has remained unheeded”.

Security Council 2286 passed in 2016 and co-sponsored by more than 80 UN member states reiterates commitment to international humanitarian law and calls on member states to be proactive in addressing attacks on health care during times of war and in adopting practical measures. The UN Secretary-General has published a list of recommended measures that would help support the implementation of Res.2286. These measures include political, diplomatic and economic pressure on parties found to be perpetrating attacks on health care, refrain from selling arms to perpetrators and investigating attacks and prosecuting violators.

On March 26, 2015, Saudi Arabia announced the start of the military operations of the Arab Coalition forces in Yemen under the name of “Operation Decisive Storm”, with the aim of restoring the legitimacy of President Abd Rabbo Mansour Hadi. This attack was a response to the coup by the Houthi forces and forces loyal to former President Ali Abdullah Saleh, which aimed to control the cities of Aden, Taiz and Marib.

The sea, land and air blockade imposed by the Saudi-led coalition has had a profound effect on the Yemeni health sector. Many medical facilities have been forced to shut, leaving injured and sick patients untreated, and basic services such as vaccination campaigns have been stopped. This lack of access to basic medical care has caused untold suffering to Yemeni civilians and has negatively impacted the average life expectancy of the Yemeni population.

## Trends and patterns

Since the Saudi-led Coalition began airstrikes, hospitals and medical have been targeted regularly, with a large increase in facilities attacked between March and December 2015. The frequency of attacks was reduced starting in January 2016 and continuing throughout that same year until December.

In 2017, airstrikes on medical facilities increased again, peaking between October and December, however the number of airstrikes on medical facilities did not reach the same frequency as observed in 2015. During 2018, airstrikes were most frequent in June and December. In 2019, the number of airstrikes targeting medical facilities was most frequent in in March and August.


<div style={{
textAlign: 'center'}}>
<TimeLine />
</div>


For the attacks on medical facilities identified within this database, Yemeni Archive has determined both alleged perpetrators as well as the area of control in which the medical facility was located.

Of the 133 attacks identified, Saudi-led coalition forces are allegedly responsible for 72 attacks, Houthis forces are allegedly responsible for 52 attacks, Al-Qaeda is allegedly responsible for three attacks, and the forces loyal to the Saudi-led coalition forces are allegedly responsible for three attacks. We could not determine the perpetrator in another three attacks.

Medical workers have been killed both by direct attacks from perpetrators or died as a result of bombings on medical facilities they were in. The 133 attacks identified by Yemeni Archive include 129 attacks in which medical facilities were directly hit.

Nine of the incidents in which medical facilities were allegedly directly hit by the Saudi-led coalition resulted in the death or injury of injured medical workers. For example, airstrikes in which Al Dourayhmi Rural Hospital was hit led to the death of many staff members.

Sixteen of the incidents in which medical facilities were allegedly directly hit by by Houthi forces resulted in the death or injury of medical workers. For example, the repeated shelling of Al Thawra hospital, which Yemeni Archive has conducted an in-depth investigation into, led to injuries among several staff members. In the city of Ad Dali, Houthi forces stormed a hospital and directly attacked medical workers inside.

Documentation obtained by Yemeni Archive indicates additional four attacks in which medical facilities were not directly targeted, but medical staff were allegedly killed as a result of alleged targeting. This includes three incidents in which ambulances were hit during Saudi-led coalition airstrikes, and one incident in which an ambulance was targeted during a Houthi-led ground assault.

In areas controlled by Houthi forces, 83 medical facilities were targeted. In areas controlled by Yemeni government forces 17 medical facilities were targeted, in areas controlled by the Saudi-led coalition 31 medical facilities were targeted. One medical facility was attacked in al-Qaeda controlled areas.
