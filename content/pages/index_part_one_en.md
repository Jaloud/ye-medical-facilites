---
page: index
name: intro
title: Key Findings
lang: en
---
## Key findings

Medical facilities and health care in Yemen have been systematically targeted by all parties to the conflict as a strategy of war and in clear contravention of international humanitarian law. Hospitals have been attacked with airstrikes and artillery shells, medical workers have been kidnapped and killed and medical facilities have been commandeered for use as military assets.

<div style={{
display: 'grid',
gridTemplateColumns: '1fr 1fr',
gridColumnGap: '4rem'
}}>

<div>
In this database, Yemeni Archive has documented 133 attacks on medical facilities and health care in Yemen since April 2014. All 133 unique incidents (based on 1,755 observations) of attacks on medical facilities include violations committed against medical facilities and/or other health workers, who are specifically offered protections under International Humanitarian Law.  This process necessitated archiving and verifying 1,755 videos, articles, and other reports from 418 sources that documented attacks on hospitals, health centers and medical centers — 596 Facebook posts, 526 Twitter posts form 342 sources, 159 YouTube videos from 76 sources, and 474 websites and news articles from 164 sources. This database has been made available to support justice and accountability efforts for human rights violations in Yemen.


It should be noted that this database may under represent the extent to which medical facilities have been targeted - Yemeni Archive has included only incidents for which documentation has been able to be identified and independently verified. While we have sought to be detailed and transparent in the presentation process, we have taken into account the need to protect those presenting these relevant documents. Taking these interests seriously, some of the content deemed sensitive has withheld to protect the security of our sources.

The added value of this database is that documentation is independently verified and structured into a standardised and searchable data ontology.  Additional descriptive information is provided to support the research of journalists, lawyers, human rights monitors and investigators for reporting, advocacy and accountability.
</div>

<div>
<bold style={{fontWeight: "800"}}> This database shows:</bold>

+ The impact of the attacks on medical facilities as a result of airstrikes and rocket attacks by various parties to the conflict.
+ Civilian victims of the airstrikes, including children and women.
+ Remnants of ammunition used in the attacks allegedly by the Saudi-led Arab coalition forces and Houthi forces.
+ Witness testimony of attacks on medical facilities, in which surviving medical staff and civilians testify through video interviews.
+ Rescue operations by humanitarian groups to assist the victims of alleged attacks against medical facilities.



## About Yemeni Archive
Yemeni Archive is a civil society initiative that has been documenting the conflict in Yemen since 2018. To date, over 499,649 videos and posts to social media have been located and preserved. Like many monitoring organisations, Yemeni Archive is unable to travel extensively within Yemen to investigate each and every attack. Relying on a network of journalists and video makers is essential to monitor, document and report on crimes committed in Yemen and preserve these pieces of evidence for justice and accountability initiatives.
</div>
</div>

## Using the database

Yemeni Archive has created the database for use by human rights workers, Yemeni observers, NGOs and INGOs and lawyers for advocacy purposes and in their quest to hold perpetrators of attacks accountable.

Please note that much of the database’s content contains graphic images, and users are advised to take precautions when reviewing materials.

However, while all efforts have been made to provide a thorough understanding of incidents and attacks, some information has been withheld to protect the security of our sources.  

To provide Yemeni Archive with new evidence or information, flag a mistake or share any questions or concerns, please contact info@yemeniarchive.org
