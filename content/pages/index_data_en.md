---
page: index
name: data
title: The Data
lang: en
---

## The Data

This database contains 11.41 GB of documentation of 133 attacks on hospitals and medical facilities in Yemen during the 2014-2019 period. This data comes from 418 sources made up of individual citizen journalists, local and international media groups, as well as NGOs and civil society organisations. It is important to note that many if not all of these sources are partisan, and thus require caution with regards to their claims.

A total of 1755 relevant videos, posts, and publications documenting attacks on hospitals and medical facilities were identified, the majority of which were published on social media pages. Yemeni Archive has not made all content documenting attacks on medical facilities public.

The sheer amount of content being created, and the near constant removals of materials from public channels means that Yemeni Archive is in a race against time to preserve important documentation of crimes committed. Content preserved and verified by the Yemeni Archive might offer the only evidence to corroborate witness testimonies of attacks on medical facilities, and to implicate potential perpetrators.

Due to Yemeni Archive’s technical infrastructure and the constant monitoring of the status of videos and channels, we have been able to identify many examples of documentation of attacks on hospitals and medical facilities in which users did not remove the content willingly. Of the 159 videos from Youtube included in the dataset, 3.95% have been made unavailable. Of the 526 Twitter posts included in the dataset, 3.14% have been made unavailable.
