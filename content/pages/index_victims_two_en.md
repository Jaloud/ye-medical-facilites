---
page: index
name: victims-two
title: Victims
lang: en
---

The number of documented victims resulting from attacks on medical facilities in Yemen (who were killed by the main actors in the conflict) is 98 people, including three women and 20 children. The number of injured is at least 191, including eight women and six children, as a result of repeated attacks on medical facilities and centers.

## Errors, corrections and feedback

The Yemeni Archive strives for accuracy and transparency of process in our reporting and presentation. That said, it is recognised that the information publicly available for particular events can, at times, be limited. Our video datasets are therefore organically maintained, and represent our best present understanding of alleged incidents. If you have new information about a particular event; if you find an error in our work - or if you have concerns about the way we are reporting our data - please do engage with us. You can reach us at info@yemeniarchive.org.
